package main

import (
	"fmt"
	"net/http"
)

func hello(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "Hello from sample metrics exporter\n")
}

func metrics(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "# HELP sample_potatos_total Total number of potatos.\n")
	fmt.Fprintf(w, "# TYPE sample_potatos_total counter\n")
	fmt.Fprintf(w, "sample_potatos_total 27\n")
}

func main() {
	http.HandleFunc("/hello", hello)
	http.HandleFunc("/metrics", metrics)
	http.ListenAndServe(":8090", nil)
}
