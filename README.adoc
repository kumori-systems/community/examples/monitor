= Monitor Component Example
Josep Maria Bernabé Gisbert <jbgisbert@kumori.systems>
v1.0.3, Nov 2022
:toc:

== Description

This example shows how the https://www.npmjs.com/package/@kumori.systems/monitor[Monitor Component] can be used as a service role to scarp metrics from other roles.

In addition to the Kumori Service Model, this examples declares a dependency with the Monitor module:

----
$ cat kmodule.cue
[...]
	dependencies: {
		"kumori.systems/kumori": {
			target: "kumori.systems/kumori/@1.1.6"
			query:  "1.1.6"
		}
		"kumori.systems/monitor": {
			target: "kumori.systems/monitor/@3.0.1"
			query:  "3.0.1"
		}
	}
[...]
----

This repository includes the several variations of a very simple example:

* `deployments/base`: the deployment only includes a simple `frontend` role and a monitor role. The frontend generates a single metric `sample_potatos_total`, which is scraped by the monitor component.
* `deployments/custom_default`: extends `deployment_base` by changing some configuration values of the default scraper.
* `deployments/extra_scraper`: adds to `deployment_custom_default` a second `worker` role using the same component than `frontend`. The `worker` role is scraped by an `extra` scraper using a role selector.
* `deployments/extra_scraper_selector`: adds to `deployment_custom_default` a second `worker` role using the same component than `frontend`. The `worker` role is scraped by an `extra` scraper using a labels selector.
* `deployments/rules`: adds to `deployment_base` a custom Prometheus rule to generate the aggregated metric `job:sample_potatos:sum`.
* `deployments/dashboard1`: adds to `deployment_base` a custom grafana dashboard based on `sample_potatos_total` metric. The dashboard has been exported disabling the `export for sharing` flag.
* `deployments/dashboard2`: adds to `deployment_base` a custom grafana dashboard based on `sample_potatos_total` metric. The dashboard has been exported enabling the `export for sharing` flag.

The services includes the following roles:

* `frontend`: generates the metric `sample_potatos_toal`. Its value is always `27`.
* `monitor`: contains the https://www.npmjs.com/package/@kumori.systems/monitor[Monitor Component]
* `worker`: appears only in `deployment_extra_scrapper` and it is a frontend twin scraped using a different configuration.

And the following server channels:

* `service`: connected to the `frontend` `entrypoint` channel. Attends to the following paths:
** `/metrics`: returns the `sample_potatos_total` metrics.
** `/hello`: says hi.
* `metrics`: used to access the monitor Prometheus.
* `dashboard`: used to access the monitor Grafana.

== Usage

Initialize `kam` context:

[source]
----
$ kam ctl init
Workspace initialized. You may want to use kumorictl config to customize configuration parameters.
$ kam ctl config -a <CLUSTER-ADMISSION-DOMAIN>
Workspace configuration updated.
$ kam ctl login <USER>
Enter Password: 
Login OK.
----

NOTE: This step is required because the monitor component is not publicly available to everyone.

Create the domains to access the monitor frontend and the internal Prometheus and Grafana services:

[source]
----
$ kam ctl register domain monitordomain --domain <MONITOR-DOMAIN>
$ kam ctl register domain promdomain --domain <PROMETHEUS-DOMAIN>
$ kam ctl register domain grafdomain --domain <GRAFANA-DOMAIN>
----

Deploy the monitor, Prometheus and Grafana inbound:

[source]
----
$ kam ctl register inbound monitorinb --domain monitordomain --cert <CLUSTER-CERTIFICATE>
$ kam ctl register inbound prominb --domain promdomain --cert <CLUSTER-CERTIFICATE>
$ kam ctl register inbound grafinb --domain grafdomain --cert <CLUSTER-CERTIFICATE>
----

Deploy the service variation of your choice:

[source]
----
$ kam service deploy -d deployments/<VARIATION> -t ./manifests monitorexample -- \
	--comment "Monitor example (variation: <VARIATION>)" \
	--wait 5m
----

Link the inbounds:

[source]
----
$ kam ctl link monitorexample:service monitorinb:inbound
$ kam ctl link monitorexample:metrics prominb:inbound
$ kam ctl link monitorexample:dashboard grafinb:inbound
----