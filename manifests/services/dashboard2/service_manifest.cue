package service

import (
  m "kumori.systems/monitor/monitor:component"
  f ".../components/frontend:component"
)

#Artifact: {
  ref: name:  "services/dashboard2"

  description: {

    config: {
      parameter: grafana: m.#GrafanaConfig
      resource: {}
    }

    role: {
      frontend: {
        artifact: f.#Artifact
        config: {
          parameter: {}
          resource: {}
          resilience: description.config.resilience
        }
      }
      monitor: {
        artifact: m.#Artifact
        config: {
          parameter: {
            grafana: description.config.parameter.grafana
          }
          resource: {}
          resilience: 0
        }
      }
    }

    srv: {
      server: {
        service: {
          protocol: "http"
          port: 80
        }
        metrics: {
          protocol: "http"
          port: 80
        }
        dashboard: {
          protocol: "http"
          port: 80
        }
      }
    }

    connect: {
      serviceconnector: {
        as: "lb"
        from: self: "service"
        to: frontend: "entrypoint": _
      }
      metrictsconnector: {
        as: "lb"
        from: self: "metrics"
        to: monitor: "metrics": _
      }
      dashboardconnector: {
        as: "lb"
        from: self: "dashboard"
        to: monitor: "dashboard": _
      }
      scraperconnector: {
        as: "lb"
        from: monitor: "scraper"
        to: frontend: "entrypoint": _
      }
    }
  }
}
