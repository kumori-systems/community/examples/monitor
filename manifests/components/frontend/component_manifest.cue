package component


#Artifact: {
  ref: name:  "components/frontend"

  description: {

    srv: {
      server: entrypoint: {
        protocol: "http"
        port:     8090
      }
      server: metrics: {
        protocol: "http"
        port:     8090
      }
    }

    size: {
      bandwidth: {
        size: 10
        unit: "M"
      }
    }

    code: frontend: {
      name: "frontend"
      image: {
        tag: "kumoripublic/examples-monitor-frontend:v1.0.5"
      }
      size: {
        memory: {
          size: 100
          unit: "M"
        }
        mincpu: 100
        cpu: {
          size: 100
          unit: "m"
        }
      }
    }
  }
}
