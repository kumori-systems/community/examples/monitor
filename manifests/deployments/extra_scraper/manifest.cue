package deployment

import (
  s ".../services/extra_scraper:service"
)

#Deployment: {
  name: "monitordep"
  artifact: s.#Artifact
  config: {
    parameter: prometheus: {
      scrape_config: {
        mydefault: {
          default: true
          instance_template: "{{`{{.Role}}`}}{{`{{.Address}}`}}"
          scrape_interval: "10s"
          scrape_timeout: "5s"
        }
        extra: {
          targets: [
            {
              roles: ["worker"]
            }
          ]
          scrape_interval: "30s"
        }
      }
      ...
    }
    resource: {}
    scale: detail: {
      frontend: hsize: 2
      worker: hsize: 2
      monitor: hsize: 1
    }
    resilience: 1
  }
}
