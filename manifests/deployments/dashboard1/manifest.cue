package deployment

import (
  s ".../services/dashboard1:service"
)

#Deployment: {
  name: "monitordep"
  artifact: s.#Artifact
  config: {
    parameter: grafana: {
      dashboards: {
        potato: _potatos_dashboard
      }
      ...
    }
    resource: {}
    scale: detail: {
      frontend: hsize: 3
      monitor: hsize: 1
    }
    resilience: 1
  }
}
