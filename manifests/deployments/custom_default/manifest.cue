package deployment

import (
  s ".../services/custom_default:service"
)

#Deployment: {
  name: "monitordep"
  artifact: s.#Artifact
  config: {
    parameter: prometheus: {
      scrape_config: {
        mydefault: {
          default: true
          instance_template: "{{`{{.Role}}`}}{{`{{.Address}}`}}"
          scrape_interval: "10s"
          scrape_timeout: "5s"
        }
      }
      ...
    }
    resource: {}
    scale: detail: {
      frontend: hsize: 2
      monitor: hsize: 1
    }
    resilience: 1
  }
}
