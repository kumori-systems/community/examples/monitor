package deployment

import (
  s ".../services/base:service"
)

#Deployment: {
  name: "monitordep"
  artifact: s.#Artifact
  config: {
    parameter: {}
    resource: {}
    scale: detail: {
      frontend: hsize: 2
      monitor: hsize: 1
    }
    resilience: 1
  }
}

