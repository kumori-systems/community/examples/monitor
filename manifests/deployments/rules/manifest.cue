package deployment

import (
  s ".../services/rules:service"
)

#Deployment: {
  name: "monitordep"
  artifact: s.#Artifact
  config: {
    parameter: prometheus: {
      rules: {
        groups: [
          {
            name: "potatos"
            rules: [
              {
                record: "job:sample_potatos:sum"
                expr: "sum by (job) (sample_potatos_total)"
              }
            ]
          }
        ]
      }
      ...
    }
    resource: {}
    scale: detail: {
      frontend: hsize: 2
      monitor: hsize: 1
    }
    resilience: 1
  }
}
