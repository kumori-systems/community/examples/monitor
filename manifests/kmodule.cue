package kmodule

{
	local:  true
	domain: "kumori.examples"
	module: "monitor"
	cue:    "0.4.2"
	version: [
		1,
		0,
		4,
	]
	dependencies: {
		"kumori.systems/kumori": {
			target: "kumori.systems/kumori/@1.1.6"
			query:  "1.1.6"
		}
		"kumori.systems/monitor": {
			target: "kumori.systems/monitor/@3.0.1"
			query:  "3.0.1"
		}
	}
	sums: {
		"kumori.systems/kumori/@1.1.6":  "jsXEYdYtlen2UgwDYbUCGWULqQIigC6HmkexXkyp/Mo="
		"kumori.systems/monitor/@3.0.1": "b+b0l3aCoYvmiWT33GYsYiNkd9UHm1aCa7yXfAYiICA="
	}
	spec: [
		1,
		0,
	]
}
