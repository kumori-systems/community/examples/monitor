#!/bin/bash

# Cluster variables
CLUSTERNAME="..."
REFERENCEDOMAIN="test.kumori.cloud"
CLUSTERCERT="cluster.core/wildcard-test-kumori-cloud"

# Service variables
DEPLOYNAME="monitordep"
INBOUNDNAME1="monitorinb"
DOMAIN1="monitordomain"
SERVICEURL1="mon-${CLUSTERNAME}.${REFERENCEDOMAIN}"
INBOUNDNAME2="prominb"
DOMAIN2="promdomain"
SERVICEURL2="prom-${CLUSTERNAME}.${REFERENCEDOMAIN}"
INBOUNDNAME3="grafinb"
DOMAIN3="grafdomain"
SERVICEURL3="graf-${CLUSTERNAME}.${REFERENCEDOMAIN}"

KUMORI_SERVICE_MODEL_VERSION="1.1.6"
KUMORI_MONITOR_VERSION="3.0.1"

KAM_CMD="kam"
KAM_CTL_CMD="kam ctl"

# Select one ...
SERVICE_VARIATION="base"
#SERVICE_VARIATION="custom_default"
#SERVICE_VARIATION="extra_scraper"
#SERVICE_VARIATION="extra_scraper_selector"
#SERVICE_VARIATION="rules"
#SERVICE_VARIATION="dashboard1"
#SERVICE_VARIATION="dashboard2"

KAM_CMD="kam"
KAM_CTL_CMD="kam ctl"

case $1 in

'refresh-dependencies')
  cd manifests
  # Dependencies are removed and added again just to ensure that the right versions
  # are used.
  # This is not necessary, if the versions do not change!
  ${KAM_CMD} mod dependency --delete kumori.systems/kumori
  ${KAM_CMD} mod dependency --delete kumori.systems/monitor
  ${KAM_CMD} mod dependency kumori.systems/kumori/@${KUMORI_SERVICE_MODEL_VERSION}
  ${KAM_CMD} mod dependency kumori.systems/monitor/@${KUMORI_MONITOR_VERSION}
  # Just for sanitize: clean the directory containing dependencies
  rm -rf ./cue.mod
  cd ..
  ;;

'create-domains')
  ${KAM_CTL_CMD} register domain $DOMAIN1 --domain $SERVICEURL1
  ${KAM_CTL_CMD} register domain $DOMAIN2 --domain $SERVICEURL2
  ${KAM_CTL_CMD} register domain $DOMAIN3 --domain $SERVICEURL3
  ;;

'deploy-inbounds')
  ${KAM_CTL_CMD} register inbound $INBOUNDNAME1 --domain $DOMAIN1 --cert $CLUSTERCERT
  ${KAM_CTL_CMD} register inbound $INBOUNDNAME2 --domain $DOMAIN2 --cert $CLUSTERCERT
  ${KAM_CTL_CMD} register inbound $INBOUNDNAME3 --domain $DOMAIN3 --cert $CLUSTERCERT
  ;;

'dry-run')
  ${KAM_CMD} process deployments/${SERVICE_VARIATION} -t ./manifests
  ;;

'deploy-service')
  ${KAM_CMD} service deploy -d deployments/${SERVICE_VARIATION} -t ./manifests $DEPLOYNAME -- \
    --comment "Monitor example (variation: ${SERVICE_VARIATION})" \
    --wait 5m
  ;;

'link')
  ${KAM_CTL_CMD} link $DEPLOYNAME:service $INBOUNDNAME1:inbound
  ${KAM_CTL_CMD} link $DEPLOYNAME:metrics $INBOUNDNAME2:inbound
  ${KAM_CTL_CMD} link $DEPLOYNAME:dashboard $INBOUNDNAME3:inbound
  ;;

'deploy-all')
  $0 create-domains
  $0 deploy-inbounds
  $0 deploy-service
  $0 link
  ;;

'update-service')
  ${KAM_CMD} service update -d deployments/${SERVICE_VARIATION} -t ./manifests $DEPLOYNAME -- \
    --comment "Monitor example updated (variation: ${SERVICE_VARIATION})" \
    --wait 5m
  ;;

'describe')
  ${KAM_CMD} service describe $DEPLOYNAME
  ;;

# Test the hello-world service
'test')
  echo
  curl https://${SERVICEURL1}/hello
  echo
  curl https://${SERVICEURL1}/metrics
  echo
  echo Prometheus is available at https://${SERVICEURL2}
  echo Grafana is available at https://${SERVICEURL3}
  ;;

'unlink')
  ${KAM_CTL_CMD} unlink $DEPLOYNAME:service $INBOUNDNAME1:inbound
  ${KAM_CTL_CMD} unlink $DEPLOYNAME:metrics $INBOUNDNAME2:inbound
  ${KAM_CTL_CMD} unlink $DEPLOYNAME:dashboard $INBOUNDNAME3:inbound
  ;;

'undeploy-service')
  ${KAM_CMD} service undeploy $DEPLOYNAME -- --wait 5m
  ;;

'undeploy-inbounds')
  ${KAM_CMD} service undeploy $INBOUNDNAME1 -- --wait 5m
  ${KAM_CMD} service undeploy $INBOUNDNAME2 -- --wait 5m
  ${KAM_CMD} service undeploy $INBOUNDNAME3 -- --wait 5m
  ;;

'delete-domains')
  ${KAM_CTL_CMD} unregister domain $DOMAIN1
  ${KAM_CTL_CMD} unregister domain $DOMAIN2
  ${KAM_CTL_CMD} unregister domain $DOMAIN3
  ;;

# Undeploy all
'undeploy-all')
  $0 unlink
  $0 undeploy-service
  $0 undeploy-inbounds
  $0 delete-domains
  ;;

*)
  echo "This script doesn't contain that command"
	;;

esac
